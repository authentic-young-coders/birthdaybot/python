import dataclasses
from dataclasses_json import dataclass_json
import discord
from typing import *
import json
import asyncio

bot = discord.Bot()
client = discord.Client()

birthday_group = bot.create_group("birthday", "Enter your birthday!")

global unsaved_birthdays 
unsaved_birthdays = {}

months = ("January February March April May June July " + \
    "August September October November December").split(" ")


class SetModal(discord.ui.Modal):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.add_item(discord.ui.InputText(label="Month", placeholder="name (e.g January) or number (e.g 1)"))
        self.add_item(discord.ui.InputText(label="Day", placeholder="number between 1-31"))

    async def callback(self, interaction: discord.Interaction):
#        embed = Embed(title="Invalid input will be ignored. If you're having issues, use /birthday set")
        respond = interaction.response.send_message
        mons = ("jan feb mar apr may jun jul aug sep oct nov dec").split(" ")
        if (month := self.children[0].value) \
                and (day_str := self.children[1].value) \
                and (day_str.isdigit()) \
                and (0 < (day := int(day_str)) <= 31) \
                and (mon := month.lower()[:3]) in mons \
                and (user := interaction.user):
            await set_birthday(respond, months[mons.index(mon)], day, user.id, user.display_name)
        else:
            response = " Your input couldn't be parsed correctly. If you're having issues, use /birthday set"
            if (user := interaction.user):
                await respond(f"<@{user.id}>" + response, ephemeral=True) 
            else:
                await respond(response, ephemeral=True)



class BirthdayView(discord.ui.View):
    @discord.ui.button(label="set")
    async def set_callback(self, btn: discord.Button, interaction: discord.Interaction):
        await interaction.response.send_modal(SetModal(title = "Set your birthday"))

    @discord.ui.button(label="show")
    async def show_callback(self, btn: discord.Button, interaction: discord.Interaction):
        if user := interaction.user:
            await show_birthday(interaction.response.send_message, user.id)
        else:
            await interaction.response.send_message("Something went wrong #show_callback", ephemeral=True)

    @discord.ui.button(label="remove")
    async def remove_callback(self, btn: discord.Button, interaction: discord.Interaction):
        if user := interaction.user:
            await remove_birthday(interaction.response.send_message, user.id)
        else:
            await interaction.response.send_message("Something went wrong #remove_callback", ephemeral=True)


class BirthdayAlreadyExistsView(discord.ui.View):
    @discord.ui.button(label="Yes", emoji="😎")
    async def yes_callback(self, btn: discord.Button, interaction: discord.Interaction):
        if user := interaction.user:
            await remove_birthday(None, user.id)
            await save_birthday(interaction.response.send_message, unsaved_birthdays[user.id])
        else:
            await interaction.response.send_message("Something went wrong #yes_callback", ephemeral=True)

    @discord.ui.button(label = "no")
    async def no_callback(self, btn: discord.Button, interaction: discord.Interaction):
        await interaction.response.send_message("Keeping old birthday", ephemeral=True)
    

@dataclass_json
@dataclasses.dataclass
class Birthday:
    month: str
    day: int
    user_id: int
    name: str


@bot.command()
async def py_ping(ctx: discord.ApplicationContext):
    await ctx.respond("pong")


@birthday_group.command(description="Show buttons for managing birthdays")
async def buttons(ctx: discord.ApplicationContext):
    await ctx.respond(view = BirthdayView(), ephemeral=True)

@birthday_group.command(description="Show your currently set birthday")
async def show(ctx: discord.ApplicationContext):
    await show_birthday(ctx.respond, ctx.author.id)

@birthday_group.command(description="Set your birthday, you will be included in the birthday list on your birthday")
async def set(
        ctx: discord.ApplicationContext, 
        month: discord.Option(str, required=True, choices=months), # type: ignore
        day: discord.Option(int, required=True, min_value=1, max_value=31) # type: ignore
):
    await set_birthday(ctx.respond, month, day, ctx.author.id, ctx.author.display_name)


@birthday_group.command(description="Remove your birthday from the bot")
async def remove(ctx: discord.ApplicationContext):
    await remove_birthday(ctx.respond, ctx.author.id)
    

async def show_birthday(respond, user_id):
    if birthday := get_birthday(user_id):
        await respond(f"<@{user_id}> Your birthday is {birthday.month} {birthday.day}", ephemeral=True)
    else:
        await respond(f"<@{user_id}> You don't have a birthday set, use /birthday set", ephemeral=True)


async def set_birthday(respond, month, day, user_id, name):
    birthday = Birthday(month, day, user_id, name)
    global unsaved_birthdays
    if get_birthday(user_id):
        unsaved_birthdays[user_id] = birthday
        await respond(f"<@{user_id}> You already have a birthday set. Do you want to overwrite it?`", view=BirthdayAlreadyExistsView(), ephemeral=True)
    else:
        await save_birthday(respond, birthday)

async def save_birthday(respond, birthday):
    modify_birthdays(lambda bs: [birthday] + bs)
    await respond(f"You set your birthday to {birthday.month} {birthday.day}", ephemeral=True)


async def remove_birthday(respond, user_id: int):
    modify_birthdays(lambda bs: [b for b in bs if b.user_id != user_id])
    if respond != None:
        await respond("Your birthday was removed from the bot", ephemeral=True)


def modify_birthdays(fn: Callable[[list[Birthday]], list[Birthday]]):
    birthdays = get_birthdays()
    with open("birthdays.json", "w") as f:
        f.write(json.dumps([dataclasses.asdict(b) for b in fn(birthdays)]))


def get_birthday(user_id: int) -> Birthday | None:
    return next(iter(b for b in get_birthdays() if user_id == b.user_id), None)


def get_birthdays() -> list[Birthday]:
    with open("birthdays.json") as f:
        return [Birthday(**j) for j in json.load(f)] #type: ignore

# @bot.event
# async def on_ready():
#     await wish_happy_birthdays()
#    schedule.every().day.at("06:44").do(lambda: asyncio.run_coroutine_threadsafe(wish_happy_birthdays(), bot.loop))

#    while True:
#        schedule.run_pending()
#        time.sleep(60)

from discord.ext import tasks, commands
from datetime import datetime, time, timedelta

@tasks.loop(hours=2)
async def wish_happy_birthdays():
    print("wish happy birthdays called")
    now = datetime.now()
    guild = bot.get_guild(432305063730610176)
    channel = guild.get_channel(524466296503664651)

    birthdays = get_birthdays()
    month = months[now.month-1]
    day = now.day

    embed = discord.Embed(title=f"{month} {day} Birthdays")
    for birthday in birthdays:
        if month == birthday.month and int(day) == birthday.day:
            embed.add_field(name="", value=birthday.name)
    await channel.send(embed=embed)

@wish_happy_birthdays.before_loop
async def before_wish():
    h = input("Hour to schedule to: ")
    m = input("Minute to schedule to: ")
    print("waiting...")
    await bot.wait_until_ready()
    print("bot ready, waiting for time")
    await wait_until(int(h), int(m))

async def wait_until(hours, minutes):
    now = datetime.now()
    some_time = time(hour=hours, minute=minutes)
    target_time = datetime.combine(now, datetime.time(datetime.combine(datetime.today(), some_time)))
    if target_time < now:
        target_time += timedelta(days=1)
    await asyncio.sleep((target_time - now).total_seconds())

if __name__ == '__main__':
    with open("tokens", "r+") as f:
        with open("birthdays.json", "r+") as bf:
            if not bf.readlines():
                bf.write("[]")

        print("1")
        wish_happy_birthdays.start()
        print("2")
        bot.run(f.readline())
        print("3")

#    t1 = threading.Thread(target=bot_thread, args=())
#    t2 = threading.Thread(target=call_birthday_thread, args=())

#    t1.start()
#    t2.start()

#    t1.join()
#    t2.join()

